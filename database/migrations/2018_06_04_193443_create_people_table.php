<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePeopleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    // CREATE SCHEMA `jobsite_db` ;

    public function up()
    {
        Schema::create('people', function (Blueprint $table) {
            $table->increments('id');
            $table->string('firstname',255);
            $table->string('lastname',255);
            $table->integer('identity_no');
            $table->year('birth_year');
            $table->integer('application_count');
            $table->date('last_application_date')->nullable();
            $table->string('email')->unique();
            $table->string('password',8);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
